class ApplicationMailer < ActionMailer::Base
  default from: "aballera@4geeks.com.ve"
  layout 'mailer'
end
